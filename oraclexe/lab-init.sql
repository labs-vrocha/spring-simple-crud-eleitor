--**********************************
--Ajuste do NLS_CHARACTERSET
--**********************************

connect sys/oracle as sysdba;
shutdown;
startup restrict;
Alter database character set INTERNAL_USE WE8ISO8859P1;
shutdown immediate;
startup;
connect system/oracle

--**********************************
--Tuning OracleXE
--**********************************

alter system set filesystemio_options=directio scope=spfile;
alter system set disk_asynch_io=false scope=spfile;

--**********************************
--Esquema eleitor
--**********************************

create tablespace eleitor datafile '/u01/app/oracle/oradata/XE/eleitor01.dbf' size 100M online;
create tablespace idx_eleitor datafile '/u01/app/oracle/oradata/XE/idx_eleitor01.dbf' size 100M;
create user eleitor identified by eleitor default tablespace eleitor temporary tablespace temp;
grant resource to eleitor;
grant connect to eleitor;
grant create view to eleitor;
grant create procedure to eleitor;
grant create materialized view to eleitor;
alter user eleitor default role connect, resource;

--**********************************
--Disabling 8080-http port
--**********************************

Exec dbms_xdb.sethttpport(0);

exit;
