package com.labs.eleitor;

import com.labs.eleitor.domain.Eleitor;
import com.labs.eleitor.repository.EleitorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;

@SpringBootApplication
public class EleitorApplication implements CommandLineRunner {

	@Autowired
	private EleitorRepository eleitorRepository;

	public static void main(String[] args) {
		SpringApplication.run(EleitorApplication.class, args);
	}

	@Override
	@Transactional
	public void run(String... args) throws Exception {
		if (eleitorRepository.count() == 0) {
			Eleitor eleitor = new Eleitor();
			eleitor.setCpf("000000");
			eleitor.setNaturalidade("Brasileira");
			eleitor.setNome("Fulano");
			eleitor.setNomeMae("Cicrana");
			eleitorRepository.save(eleitor);
		}
	}

}
