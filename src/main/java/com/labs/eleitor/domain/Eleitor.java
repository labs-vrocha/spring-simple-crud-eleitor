package com.labs.eleitor.domain;

import javax.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "ELEITOR")
public class Eleitor {

    @Id
    @Column(updatable = false, nullable = false)
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "SEQ_ELEITOR")
    @SequenceGenerator(initialValue = 1, allocationSize = 1, name = "SEQ_ELEITOR", sequenceName = "SEQ_ELEITOR")
    private Long id;

    @Column
    private String nome;

    @Column
    private String cpf;

    @Column
    private String nomeMae;

    @Column
    private String naturalidade;
}
