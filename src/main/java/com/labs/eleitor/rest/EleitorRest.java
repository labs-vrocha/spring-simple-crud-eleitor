package com.labs.eleitor.rest;

import com.labs.eleitor.domain.Eleitor;
import com.labs.eleitor.repository.EleitorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("eleitores")
public class EleitorRest {

    @Autowired
    private EleitorRepository eleitorRepository;

    @GetMapping
    public List findAll(){
        return eleitorRepository.findAll();
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Eleitor> findById(@PathVariable Long id){
        Optional<Eleitor> eleitor = eleitorRepository.findById(id);
        if(eleitor.isPresent()){
            return new ResponseEntity<Eleitor>(eleitor.get(), HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
